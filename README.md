# Your pip freedom

![GNU GPLv3+](https://www.gnu.org/graphics/gplv3-127x51.png)

Detects all non-free PyPI packages and removes them. 

"""
    "Your pip freedom" package helps to find and delete all non-free
    packages, installed by pip from PyPI and other repositories.
    Copyright (C) 2023 Stepan Zubkov <stepanzubkov@florgon.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from importlib.metadata import metadata, packages_distributions
from typing import Optional
import subprocess

import click

from your_pip_freedom.osi_approved import OSI_APPROVED


@click.group()
def main():
    pass


@main.command()
def scan():
    """
    Scans all pip packages and deletes proprietary packages.
    """
    all_packages_names = {v[0] for v in packages_distributions().values()}
    licenses = {}
    non_free_packages = 0
    removed_non_free_packages = 0
    for package in all_packages_names:
        license = get_license(package)
        if not license:
            click.secho(f"Package '{package}' has unknown license. "
                        f"Check https://pypi.org/project/{package} for information about license. "
                        f"If it has proprietary license, "
                        f"uninstall this package with following command:\n"
                        f"\tpip uninstall {package}", fg="yellow")
        elif is_proprietary_license(license):
            non_free_packages += 1
            click.secho(f"Package '{package}' has proprietary license '{license}'.", fg="red")
            if click.confirm("Delete this package?"):
                subprocess.run(["pip", "uninstall", "-y", package])
                removed_non_free_packages += 1
        else:
            license = license.split(" :: ")[-1]
            licenses[license] = licenses.get(license, 0) + 1

    click.secho(f"Scanning finished. Found {len(all_packages_names)} free packages:", fg="green")
    for license_name, count in sorted(licenses.items(), key=lambda x: -x[1]):
        click.echo(f" - {count} packages with '{license_name}' license")
    click.secho(f"{non_free_packages} non-free packages found and "
                f"{removed_non_free_packages} of them removed.", fg="green")


def get_license(package: str) -> Optional[str]:
    license_field = None
    for k, v in metadata(package).items():
        if k == "Classifier" and v.startswith("License :: "):
            return v.replace("License :: ", "")
        if k == "License":
            license_field = v

    return license_field


def is_proprietary_license(license: str) -> bool:
    if (
        license.startswith("CC0")
        or license.startswith("OSI Approved")
        or license.startswith("Public Domain")
    ):
        return False
    for license_id in OSI_APPROVED:
        if license_id in license.replace(" ", ""):
            return False

    return True


if __name__ == "__main__":
    main()
